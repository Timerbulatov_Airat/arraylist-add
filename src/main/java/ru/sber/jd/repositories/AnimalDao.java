package ru.sber.jd.repositories;

import ru.sber.jd.dto.AnimalDto;

import java.util.ArrayList;
import java.util.List;

public class AnimalDao {

    public List<AnimalDto> select() {
        List<AnimalDto> animals = new ArrayList<AnimalDto>();
        animals.add(AnimalDto.builder()
                .name("Барсик")
                .age(2)
                .animal("Cat")
                .owner("Иванов Иван")
                .build());

        animals.add(AnimalDto.builder()
                .name("Бобик")
                .age(5)
                .animal("Dog")
                .owner("Сидоров Максим")
                .build());

        animals.add(AnimalDto.builder()
                .name("Лёня")
                .age(3)
                .animal("Cat")
                .owner("Макаров Андрей")
                .build());

        return animals;

    }
}
