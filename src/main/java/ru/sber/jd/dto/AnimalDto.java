package ru.sber.jd.dto;

import lombok.*;

@Builder
@Value

public class AnimalDto {
    private String name;
    private int age;
    private String animal;
    private String owner;
}
